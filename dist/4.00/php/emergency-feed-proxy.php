<?php
    ##########################################################
    # CFA and RFS fire incident feed proxy and cache
    # author:     pborbely@fairfaxmedia.com.au | @datafunk
    # born:       October 2014
    # version:    4.00
    ##########################################################


    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Credentials: true");
    header('Content-Type: application/json');
    header("Cache-Control: no-cache, must-revalidate"); // HTTP/1.1
    header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past

    if (isset($_GET["s"]) && isset($_GET["t"]) ){

        $geojson;
        $state      = $_GET["s"]; // nsw | vic
        $extension  = ''; // indicates if feed is proper geojson
        $cache_life = $_GET["t"];  // cache lifetime in seconds
        $callback = 'PEZ.disasterMaps.vic.fire'; //! MUST BE set explicitly as CFA service appends further strings

        if ($cache_life < 60){
            $cache_life = 60;
        }

        $feed = ''; // feed URL
        if ($state == 'nsw'){
            $feed = 'http://www.rfs.nsw.gov.au/feeds/majorIncidents.json';
            $extension = 'geojson';
        } elseif ($state == 'vic' && isset($_GET["callback"]) ){

            $feed = 'https://data.emergency.vic.gov.au/Show?pageId=getCapWarningJSON&callback='.$callback;
            $extension = 'json';

        }

        $filename = $_SERVER['DOCUMENT_ROOT'].'/interactive/shared/disaster-maps/feeds/'.$state.'.'.$extension;


        $target_dir = "../../feeds";
        # Check if target dir exist
        if (!file_exists($target_dir)){
           mkdir($target_dir, 0755, true);
        }

        $feed_file = file_get_contents($feed);
        $cache_file = $feed;

        if ($state == 'vic') {
            $prepend = $callback."(\n\n";
            $append = "\n)";
            $prepend .= $feed_file;
            $prepend .= $append;
            // $feed_file = $prepend;
        }

        if ( file_exists($filename) ){

            if ( time() - filemtime($filename) >= $cache_life ){
                // echo "---\n1\nExists, but expired\n---\n";
                file_put_contents( $filename, $feed_file);
                echo $feed_file;
                exit;
            } else {
                // echo "---\n2\nExists and valid\n---\n";
                readfile($filename);
                exit;
            }
        } else {
            // echo "---\n3\nNew, need to write it\n---\n";
            file_put_contents( $filename, $feed_file);
            readfile($filename);
            exit;
        }

    }



?>
