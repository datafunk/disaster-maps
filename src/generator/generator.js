PEZ.disasterMaps.generator = (function() {
    // console.info('generator loaded');

    // document.location.href = 'http://dev.ddj.smh.com.au/utils/disaster-map-generator/?s=nsw&t=300&c=32.78727452695549,147.85400390625&z=6';

    /*! REQUIRED Query parameters
     *   s = state: nsw | vic
     *   t = cached time in seconds
     *   c = coordinates: lat,lng
     *   z = zoom
     *   OPTIONAL Query parameter
     *   config = 0 | 1
     */

    /*
        Sample NSW query string:
        ?s=nsw&t=60&c=-32.78727452695549,147.85400390625&z=6

        Sample VIC string
        ?s=vic&t=60&c=-36.43012234551578,144.20757293701172&z=6
    */


    function updateQueryStringParameter(url, param, value) {
        var regex = new RegExp('(' + param + '=)[^\&]+');
        return url.replace(regex, '$1' + value);
    }


    var QueryString = function() {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [query_string[pair[0]], pair[1]];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }
        return query_string;
    }();


    var state = QueryString.s;
    document.forms[0].state.value = state;
    var _href = document.location.href;



    $('input').on('click', function(e) {

        state = document.forms[0].state.value;

        if (QueryString.s === undefined) {
            _href = _href + '?s=' + state;
        } else {
            _href = updateQueryStringParameter(_href, 's', state);
        }

        if (QueryString.c === undefined) {
            if (state === "nsw") {
                _href = _href + "&c=32.78727452695549,147.85400390625";
            } else if (state === "vic") {
                _href = _href + "&c=-36.43012234551578,144.20757293701172";
            }
        } else {

            if (QueryString.c === undefined) {
                if (state === "nsw") {
                    _href = _href + "&c=32.78727452695549,147.85400390625";
                } else if (state === "vic") {
                    _href = _href + "&c=-36.43012234551578,144.20757293701172";
                }
            } else {
                if (state === "nsw") {
                    _href = updateQueryStringParameter(_href, 'c', "32.78727452695549,147.85400390625");
                } else if (state === "vic") {
                    _href = updateQueryStringParameter(_href, 'c', "-36.43012234551578,144.20757293701172");
                }
            }


        }

        if (QueryString.t === undefined) {
            _href = _href + "&t=300"
        } else {
            _href = updateQueryStringParameter(_href, 't', 300);
        }

        if (QueryString.z === undefined) {
            _href = _href + "&z=6";
        } else {
            _href = updateQueryStringParameter(_href, 'z', 6);
        }

        document.location.href = _href;
    });

    $('#get_embed').on('click', function(e) {
        // console.log(e);
        $('#embed_code').val(iframe_embed);
        $('#embed_code').slideDown(300);

        $('#embed_code').on('click', function(){
            this.focus();
            this.select();
        });
    });


    /* To produce a new instance */
    map = PEZ.disasterMaps.map;
    $('#grab_config').on('click', function(){
        var currentCentre = map.getCenter();
        currentCentre = currentCentre.lat + ',' + currentCentre.lng;
        var currentZoom = map.getZoom();

        _href = updateQueryStringParameter(_href, 'c', currentCentre);
        _href = updateQueryStringParameter(_href, 'z', currentZoom);
        document.location.href = _href;
    });


    var iframe_embed = '\n';
    iframe_embed += '<!--//start interactive-->\n';
    iframe_embed += '<div style="margin:0;width:100%;max-width:620px;margin:0 auto;">\n\t';
    iframe_embed += '<iframe frameborder="0" height="620px" id="pez_iframe" marginheight="0" marginwidth="0" scrolling="no" src="';
    // URL comes here
    iframe_embed += 'http://ddj.smh.com.au/interactive/shared/disaster-maps/{{ VERSION }}/article.html';
    iframe_embed += '?s=';
    iframe_embed += QueryString.s;
    iframe_embed += '&t=';
    iframe_embed += QueryString.t;
    iframe_embed += '&c=';
    iframe_embed += QueryString.c;
    iframe_embed += '&z=';
    iframe_embed += QueryString.z;
    iframe_embed += '" style="width:100%;max-width:620px;border:none;margin:0 auto;padding:0" width="100%"></iframe>\n';
    iframe_embed += '</div>\n';
    iframe_embed += '<script type="text/javascript" src="http://ddj.smh.com.au/interactive/libraries/PEZ.common/iframeResizer.min.js"></script>\n';
    iframe_embed += '<script type="text/javascript">\n\t';
    iframe_embed += 'iFrameResize({resizedCallback : function(messageData){}},"#pez_iframe");\n';
    iframe_embed += '</script>\n';
    iframe_embed += '<!--//end interactive-->\n';



})();
