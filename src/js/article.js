/*!
    UGC Disaster-Maps
    author:     pborbely@fairfaxmedia.com.au | @datafunk
    born:       October 2014
    version:    4.xx
*/

var PEZ = PEZ || {}; // Define namespace

PEZ.disasterMaps = (function() {

    var path = './php/'; // use relative path for version controlled directory access
    var parser = 'emergency-feed-proxy.php';

    /*! REQUIRED Query parameters
    *   s = state: nsw | vic
    *   t = cached time in seconds
    *   c = coordinates: lat,lng
    *   z = zoom
    *   OPTIONAL Query parameter
    *   config = 0 | 1
    */

    /*
        Sample NSW query string:
        ?s=nsw&t=60&c=-32.713,147.766&z=6

        Sample VIC string
        ?s=vic&t=60&c=-37.43,145.10&z=6
    */

    var QueryString = function() {
        // This function is anonymous, is executed immediately and
        // the return value is assigned to QueryString!
        var query_string = {};
        var query = window.location.search.substring(1);
        var vars = query.split("&");
        for (var i = 0; i < vars.length; i++) {
            var pair = vars[i].split("=");
            // If first entry with this name
            if (typeof query_string[pair[0]] === "undefined") {
                query_string[pair[0]] = pair[1];
                // If second entry with this name
            } else if (typeof query_string[pair[0]] === "string") {
                var arr = [query_string[pair[0]], pair[1]];
                query_string[pair[0]] = arr;
                // If third or later entry with this name
            } else {
                query_string[pair[0]].push(pair[1]);
            }
        }
        return query_string;
    }();

    var c = QueryString.c.split(',');
    var map_config = {
        state: QueryString.s,
        cachetime: QueryString.t,
        coordinates: {
            lat: parseFloat(c[0]),
            lng: parseFloat(c[1])
        },
        zoom: QueryString.z,
        minzoom: 5,
        maxzoom: 17,
    }

    var feeds = {
        nsw: {
            fsrc: "/interactive/shared/disaster-maps/{{ VERSION }}/php/emergency-feed-proxy.php?s=nsw&t=" + map_config.cachetime,
            dtype: "json",
            web: "http://www.rfs.nsw.gov.au/fire-information/fires-near-me",
            agent: "RFS"
        },
        vic: {
            // fsrc: "/interactive/shared/disaster-maps/{{ VERSION }}/php/emergency-feed-proxy.php?s=vic&t=" + map_config.cachetime + "&callback=PEZ.disasterMaps.vic.fire",
            fsrc: "/interactive/shared/disaster-maps/{{ VERSION }}/php/emergency-feed-proxy.php?s=vic&t=" + map_config.cachetime,
            dtype: "jsonp",
            web: "http://www.cfa.vic.gov.au/",
            agent: "CFA"
        }
    }

    var map = L.map('pez_map', {
        scrollWheelZoom: true,
        dragging: true,
        zoomControl: true,
        doubleClickZoom: false,
        attributionControl: true,
        minZoom: map_config.minzoom,
        maxZoom: map_config.maxzoom
    }).setView([map_config.coordinates.lat, map_config.coordinates.lng], map_config.zoom);

    // L.tileLayer(
    //     'http://a.tiles.mapbox.com/v3/fairfax.map-ki9vu21i/{z}/{x}/{y}.png', {
    //         maxZoom: map_config.maxzoom
    //     }).addTo(map);
    L.tileLayer(
        'http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
            maxZoom: map_config.maxzoom
        }).addTo(map);

    var attribution = 'Map: Andy Ball + <a href="http://twitter.com/datafunk" target="_blank">@datafunk</a>';
    attribution += ' | Source: <a href="' + feeds[map_config.state].web + '" target="_blank">' + feeds[map_config.state].agent + '</a>';
    map.attributionControl.setPrefix(attribution);


    function loadFeed(feed, dtype) {

        $.ajax({
                url: feed,
                dataType: dtype
            })
            .done(function(data) {
                // console.info("-- done --");
                // console.log(data);
                // console.log(typeof(data));
                if (map_config.state === 'nsw') {
                    PEZ.disasterMaps.nsw(data);
                } else if (map_config.state === 'vic') {
                    PEZ.disasterMaps.vic.fire(data);
                }
                // PEZ.disasterMaps.nsw(data);
            })
            .fail(function(err) {
                // console.error(err);
                // console.error(err.responseText);
                if (err && err.status === 417) {
                    var data = err.responseText;
                    PEZ.disasterMaps.handle_stale_feed(data);
                }
                // for test only
                // PEZ.disasterMaps.handle_stale_feed();
            });
        // .always(function() {
        //     console.info("-- always --");
        // });

    }


    function handle_stale_feed(data) {
        $('#pez_error').fadeIn(300);
        $('#pez_desc').hide();
        $('#pez_error p')[0].textContent = 'There was an error fetching the latest fire alerts! We\'re displaying cached data!';
        $('#pez_error p')[1].textContent = 'If you are in an emergency situation, seek alternative information sources, such as your local radio.';
        $('#pez_error button').on('click', function() {
            $('#pez_error').fadeOut(300);
        });

        if (map_config.state === 'nsw') {
            // error responseText  string did not validate as geojson, parse it
            data = JSON.parse(data);
            PEZ.disasterMaps.nsw(data);
        }
        // else if (map_config.state === 'vic') {
        //     PEZ.disasterMaps.vic.fire();
        // }

    }

    $('.pez_closebtn').on('click', function() {
        $('#pez_desc').fadeOut(300);
    });


    document.addEventListener('touchmove', function(e) {
        e.preventDefault();
    }, false);


    document.addEventListener('DOMContentLoaded', function(event) {
        if (map_config.state === "vic") {
            loadFeed(feeds.vic.fsrc, feeds.vic.dtype);
            PEZ.disasterMaps.vic();
        } else {
            loadFeed(feeds.nsw.fsrc, feeds.nsw.dtype);
        }

    }, false);


    /* To produce a new instance */
    /* update own loaction with current centre and zoom if ?config = 1 */
    /* moved this functionality into /utils/disaster-map-generator/generator.js */
    // if (parseInt(QueryString.config) === 1){

    //     function updateQueryStringParameter(url, param, value){
    //         var regex = new RegExp('('+param+'=)[^\&]+');
    //         return url.replace( regex , '$1' + value);
    //     }

    //     map.on('click', function(){
    //         var currentCentre = map.getCenter();
    //         currentCentre = currentCentre.lat + ',' + currentCentre.lng;
    //         var currentZoom = map.getZoom();
    //         var _href = document.location.href;
    //         _href = updateQueryStringParameter(_href, 'c', currentCentre);
    //         _href = updateQueryStringParameter(_href, 'z', currentZoom);
    //         document.location.href = _href;
    //     });
    // }

    // public properties and functions
    return {
        L: L,
        map: map,
        map_config: map_config,
        handle_stale_feed: handle_stale_feed,
        version: "{{ VERSION }}"
    }

})();


PEZ.disasterMaps.vic = (function() {

    map = PEZ.disasterMaps.map;
    L = PEZ.disasterMaps.L;
    var current;
    var editorial = [];

    var legend = L.control({
        position: 'bottomleft'
    });

    legend.onAdd = function(map) {

        var leg = L.DomUtil.create('div', 'legend');
        leg.innerHTML += '<div class="pez_legend vic"><i class="advice"></i>Advice</div>';
        leg.innerHTML += '<div class="pez_legend vic"><i class="watchAct"></i>Watch and Act</div>';
        leg.innerHTML += '<div class="pez_legend vic"><i class="warning"></i>Emergency Warning</div>';

        return leg;
    };

    legend.addTo(map);

    $('#pez_inc_desc').addClass('vic');


    PEZ.disasterMaps.vic.fire = function(alerts) {
        // console.log(alerts);

        $.each(alerts.alerts, function(x, a) {
            $.each(a.info, function(y, b) {
                var warning = '';
                var description = '';
                warning = b.headline;
                description = $.htmlClean(b.description, {
                    removeAttrs: ['style', 'class'],
                    removeTags: ['font', 'span'],
                    replace: [['div'], 'p']
                });

                $.each(b.area, function(z, c) {
                    if (c.polygon) {

                        // what htmlClean() failed to achieve
                        description = description.replace(/<div><\/div>/g, '');
                        description = description.replace(/<div><div>/g, '<div>');
                        description = description.replace(/<\/div><\/div>/g, '</div>');
                        description = description.replace(/<div>/g, '<p>');
                        description = description.replace(/<\/div>/g, '</p>');
                        description = description.replace(/<>/g, '');
                        editorial.push(description);
                        var eid = editorial.length;

                        var polygon = [];
                        polygon = c.polygon.toString().split(' ').map(function(item) {

                            var latLng = item.split(',');
                            var lat = parseFloat(latLng[0]);
                            var lng = parseFloat(latLng[1]);
                            var coordinates = [];
                            coordinates.push(lat);
                            coordinates.push(lng);
                            return coordinates;

                        });

                        var geometry = L.polygon(polygon, {
                            weight: 1,
                            opacity: 1,
                            color: setColor(warning),
                            fillColor: setColor(warning),
                            fillOpacity: 0.8
                        }).addTo(map);

                        geometry.on('click', function() {
                            $('#pez_desc').fadeIn(300);
                            $('#pez_inc_desc').html(editorial[eid - 1]);
                        });

                    } else {
                        var latLng = c.circle.toString();
                        latLng = latLng.split(',');
                        var lat = parseFloat(latLng[0]);
                        var lng = parseFloat(latLng[1]);

                        var incidentIcon = L.icon({
                            shadowUrl: 'img/shadow.png',
                            iconUrl: setMarker(warning),
                            iconSize: [60, 60],
                            shadowSize: [60, 60],
                            iconAnchor: [30, 60],
                            // point of the icon which will correspond to marker's location
                            popupAnchor: [0, -30]
                                // point from which the popup should open relative to the iconAnchor
                        });

                        var marker = L.marker([lat, lng], {
                            icon: incidentIcon
                        }).addTo(map);
                        marker.bindPopup(c.areaDesc);
                    }
                });
            });
        });
    }


    function setMarker(warning) {
        return warning == 'Advice' ? 'img/advice_vic.png' :
            warning == 'Watch and Act' ? 'img/watch_vic.png' :
            warning == 'Emergency Warning' ? 'img/emergency_vic.png' : 'img/emergency_vic.png';
    }

    function setColor(warning) {
        return warning == 'Advice' ? '#eddb15' :
            warning == 'Watch and Act' ? '#ed9a15' :
            warning == 'Emergency Warning' ? '#ed1515' : '#000000';
    }

});


PEZ.disasterMaps.nsw = (function(data) {

    map = PEZ.disasterMaps.map;
    L = PEZ.disasterMaps.L;

    var legend = L.control({
        position: 'bottomleft'
    });

    legend.onAdd = function(map) {
        var leg = L.DomUtil.create('div', 'legend');
        leg.innerHTML += '<div class="pez_legend nsw"><i class="na"></i>Not Applicable</div>';
        leg.innerHTML += '<div class="pez_legend nsw"><i class="advice"></i>Advice</div>';
        leg.innerHTML += '<div class="pez_legend nsw"><i class="watchAct"></i>Watch and Act</div>';
        leg.innerHTML += '<div class="pez_legend nsw"><i class="warning"></i>Emergency Warning</div>';
        return leg;
    };
    legend.addTo(map);


    addGeo(data);
    function addGeo(data) {

        function cloroplethStyle(feature) {
            // console.log(feature);
            if (feature.properties.category.toLowerCase() === "not applicable") {
                return {
                    weight: 0.5,
                    opacity: 0,
                    color: "grey",
                    fillColor: "white",
                    fillOpacity: 0,
                    strokeWeight: 1,
                    strokeColor: "grey"
                }
            } else if (feature.properties.category.toLowerCase() === "advice") {
                return {
                    weight: 0.5,
                    opacity: 0.3,
                    color: "blue",
                    fillColor: "blue",
                    fillOpacity: 0.3,
                    strokeWeight: 1,
                    strokeColor: "blue"
                }
            } else if (feature.properties.category.toLowerCase() === "watch and act") {
                return {
                    weight: 1,
                    opacity: 0.3,
                    color: "#F89B02",
                    fillColor: "#F89B02",
                    fillOpacity: 0.3,
                    strokeWeight: 2,
                    strokeColor: "#F89B02"
                }
            } else if (feature.properties.category.toLowerCase() === "emergency warning") {
                return {
                    weight: 1,
                    opacity: 0.3,
                    color: "#BB0008",
                    fillColor: "#BB0008",
                    fillOpacity: 0.3,
                    strokeWeight: 2,
                    strokeColor: "#BB0008"
                }
            }

        }

        function onMapClick() {
            var feature = this.feature;

            $('#pez_desc').fadeIn(300);
            $('#pez_desc h2').html(feature.properties.title);

            var alert_class = feature.properties.category;
            if (alert_class.toLowerCase() === 'not applicable') {
                alert_class = 'na';
            } else if ((alert_class.toLowerCase() === 'advice')) {
                alert_class = 'advice';
            } else if ((alert_class.toLowerCase() === 'watch and act')) {
                alert_class = 'watchAct';
            } else {
                alert_class = 'warning';
            }

            $('#pez_desc h2').removeClass('na advice watchAct warning');
            $('#pez_desc h2').addClass(alert_class);
            $('#pez_inc_desc').html(feature.properties.description);
        }

        geojson = L.geoJson(data, {

            pointToLayer: function(feature, latlng) {

                var feat_icon;
                if (feature.properties.category.toLowerCase() === 'not applicable') {
                    feat_icon = 'img/Not_Applicable.png';
                } else if (feature.properties.category.toLowerCase() === 'advice') {
                    feat_icon = 'img/Advice.png';
                } else if (feature.properties.category.toLowerCase() === 'watch and act') {
                    feat_icon = 'img/WatchandAct.png';
                } else if (feature.properties.category.toLowerCase() === 'emergency warning') {
                    feat_icon = 'img/Emergency_Warning.png';
                }

                var myicon = L.icon({
                    iconSize: [26, 26],
                    iconUrl: feat_icon
                });
                return L.marker(latlng, {
                    icon: myicon
                });


            },
            style: cloroplethStyle,
            onEachFeature: function(feature, layer) {
                layer._leaflet_name = feature.properties.NAME;
                layer.on({
                    click: onMapClick,
                    // mouseover: highlight,
                    // mouseout: resetStyle
                });
            }
        }).addTo(map);


        var southWest = L.latLng(geojson.getBounds()._southWest),
            northEast = L.latLng(geojson.getBounds()._northEast),
            bounds = L.latLngBounds(southWest, northEast);

        // increase bounds by 0.2 degrees to fit icons on map properly
        bounds._northEast.lat += 0.2 // plus for northerly padding
        bounds._northEast.lng += 0.2 // plus for easterly padding
        bounds._southWest.lat -= 0.2 // minus for southerly padding
        bounds._southWest.lng -= 0.2 // minus for westerly padding


        map.setMaxBounds(bounds, {
            padding: [20, 20]
        });

    }

});
