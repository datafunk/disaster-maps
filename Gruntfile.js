module.exports = function(grunt) {

    var bp_banner = '/*\n<%= pkg.name %>\nversion:\t<%= pkg.version %>\n\nBorn:\t\t<%= pkg.born %>\nAuthor:\t\t<%= pkg.author.name %> <%= pkg.author.handle %>\nUpdated: \t<%= grunt.template.today("yyyy-mm-dd") %> \n*/\n';

    // ===========================================================================
    // CONFIGURE GRUNT ===========================================================
    // ===========================================================================
    grunt.initConfig({

        // get the configuration info from package.json ----------------------------
        pkg: grunt.file.readJSON('package.json'),


        // configure jshint to validate js files -----------------------------------
        jshint: {
            // options: {
            //     reporter: require('jshint-stylish')
            // },
            all: ['Grunfile.js', 'src/*.js']
        },


        // configure uglify to minify app.js files -------------------------------------
        uglify: {
            options: {
                banner: bp_banner,
                mangle: false,
                sourceMap: true
            },
            build: {
                files: {
                    'dist/<%= pkg.version %>/js/article.min.js': 'src/js/article.js'
                }
            }
        },

        // configure cssmin to minify css files ------------------------------------
        cssmin: {
            options: {
                // banner: '/*\n <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> \n*/\n'
                banner: bp_banner
            },
            build: {
                files: {
                    'src/css/leaflet.min.css': 'src/css/leaflet.css',
                    'src/css/article.min.css': 'src/css/article.css'
                }
            }
        },

        'string-replace': {
            version: {
                files: {
                    // dest:src
                    'src/generator/index.html':'src/generator/index.html.ejs',
                    'dist/generator/generator.js':'src/generator/generator.js',
                    'src/article.html':'src/article.html.ejs',
                    'dist/<%= pkg.version %>/js/article.min.js':'dist/<%= pkg.version %>/js/article.min.js',
                    'dist/<%= pkg.version %>/js/leaflet.map.min.js':'dist/<%= pkg.version %>/js/leaflet.map.min.js',
                    'dist/<%= pkg.version %>/php/emergency-feed-proxy.php':'src/php/emergency-feed-proxy.php',
                    'src/iframe.html':'src/iframe.html.ejs'
                },
                options: {
                    replacements: [{
                        pattern: /{{ VERSION }}/g,
                        replacement: '<%= pkg.version %>'
                    }]
                }
            }
        },

        //  copy files and folder  ----------------------------------------------
        copy: {
            // copy to dist/
            dist_gen: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    // src: ['**/generator/**', '!**/generator/*.ejs', '!**/generator/server.*', '!**/generator/generated_html/*.html'],
                    src: ['generator/*','generator/img/*.png', '!**/generator/*.ejs', '!**/generator.js'],
                    dest: 'dist/'
                }]
            },
            dist_php: {
                files: [{
                    expand: true,
                    cwd: 'src/php/',
                    src: ['**/*.php'],
                    dest: 'dist/<%= pkg.version %>/php/'
                }]
            },
            dist_html: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    // src: ['**/*', '!**/generator/**', '!**/db/**', '!**/feeds/**'],
                    src: ['*.html'],
                    dest: 'dist/<%= pkg.version %>/'
                }]
            },
            dist_img: {
                files: [{
                    expand: true,
                    cwd: 'src/img/',
                    // src: ['**/*', '!**/generator/**', '!**/db/**', '!**/feeds/**'],
                    src: ['**/*'],
                    dest: 'dist/<%= pkg.version %>/img/'
                }]
            },
            // copy to ddjv2
            ddj_utils: {
                nonull: true,
                expand: true,
                cwd: 'dist/generator/',
                src: ['**/*', 'img/*.png', '!**/generator/*.ejs', '!**/generator/server.*', '!**/generator/generated_html/*.html'],
                dest: '<%= pkg.util_path %>/'
            },
            ddj_shared: {
                nonull: true,
                expand: true,
                cwd: 'dist/<%= pkg.version %>/',
                src: ['**/*'],
                dest: '<%= pkg.dist_path %>/<%= pkg.version %>/'
            }
        },


        // configure watch to auto update ------------------------------------------
        watch: {
            stylesheets: {
                files: ['src/**/*.css'],
                tasks: ['cssmin']
            },
            scripts: {
                files: 'src/**/*.js',
                tasks: ['default']
                // options: {
                //     spawn: false,
                //     livereload: false,
                // },
            },
            php: {
                files: ['src/php/*'],
                tasks: ['default']
            },
            html: {
                files: ['src/generator/*', 'src/generator/.*'],
                tasks: ['replace', 'copy']
            }
        },

        // configure to concat all the files ------------------------------------

        concat: {
            options: {
                banner: bp_banner,
                separator: ';\n\n'
            },
            dist_js: {
                // concat in order
                src: ['src/libs/jquery-1.11.0.min.js', 'src/libs/jquery.htmlClean.min.js', 'src/libs/leaflet.js'],
                dest: 'dist/<%= pkg.version %>/js/libs.min.js'
            },
            dist_css: {
                // concat in order
                src: ['src/css/leaflet.min.css', 'src/css/article.min.css'],
                dest: 'dist/<%= pkg.version %>/css/article.styles.libs.min.css'
            }
        },


        // configure compression
        compress: {
            main: {
                options: {
                    archive: './dist/<%= pkg.version %>.zip',
                    pretty: true
                },
                expand: true,
                cwd: './dist/<%= pkg.version %>/',
                src: ['**/css/*', '**/js/*', '**/img/*', 'article.html'],
                dest: './'
            }
        }

    });

    // ===========================================================================
    // LOAD GRUNT PLUGINS ========================================================
    // ===========================================================================
    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-copy');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-jshint');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-compress');
    grunt.loadNpmTasks('grunt-string-replace');
    // grunt.loadNpmTasks('grunt-contrib-less');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    // grunt.loadNpmTasks('grunt-processhtml');

    // ===========================================================================
    // CREATE TASKS ==============================================================
    // ===========================================================================

    grunt.registerTask('minify', ['uglify', 'cssmin']);
    grunt.registerTask('zip', 'compress');
    grunt.registerTask('replace', 'string-replace');
    // grunt.registerTask('gen_only', ['string-replace', 'copy:dist_gen']);

    grunt.registerTask('default', ['replace', 'cssmin', 'minify','replace', 'concat', 'zip', 'copy']);

};

// configure to concat all the files ------------------------------------

// concat: {
//     options: {
//         separator: ';\n'
//     },
//     dist: {
//         // concat in order
//         // src: ['src/lib/bootstrap.min.js', 'src/lib/ie10-viewport-bug-workaround.js', 'src/lib/date.js', 'src/lib/angular.min.js', 'src/lib/rzslider.min.js', 'src/lib/leaflet-heat.js', 'src/lib/angular-leaflet-directive.min.js', 'src/lib/angular-flot-mod.js'],
//         src: ['src/lib/*.js'],
//         dest: 'dist/lib/libs.min.js'
//     }
// },

// Replace parts in index file:
// processhtml: {
//     build: {
//         files: {
//             'dist/index.html': ['src/index.html']
//         }
//     }
// },
