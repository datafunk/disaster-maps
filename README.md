# Disaster Maps #
## version 4 ##
### changelog 4.01 ###

- proxy now handles failure of external feeds and returns cached content with HTTP statusCode 417 (Expectation Failed)
- front-end handles above 417 errors
- front-end has been refactored
- v4.x article embed generator: http://ddj.smh.com.au/utils/disaster-map-generator/


### changelog 4.00 ###

- no UGC component
- simplified proxy-cache
- rewritten to use leaflet.js
- rewritten to use NSW RFS geojson and VIC CFA rubbish.json (client side parsing - thanks Andy!)
- versioned path to assets
- migrated to ddj AWS box with new location: ```/interactive/shared/disaster-maps/x.xx```
- URL query based simple config
- single HTML file across multiple instances with different configurations (focused location / feed)
- Workflow: Grunt


### How to use ###

This map is designed to be embedded into articles via ```<iframe>```.
Simply append the following parameters to the iframe's src to configure its behaviour:

- ```s``` = state ```nsw | vic```
- ```t``` = cache-time in seconds, mimimum 60 (values set to less than 60 are ignored and feed will be cached for 60 seconds)
- ```c``` = coordinates as ```lat,lng```
- ```z``` = zoom as integer

E.g

- http://stageddj.smh.com.au/interactive/shared/disaster-maps/4.00.35/article.html?s=vic&t=60&c=-37.3,144.6&z=6
- http://stageddj.smh.com.au/interactive/shared/disaster-maps/4.00.35/article.html?s=nsw&t=300&c=-32.713,147.5&z=6

__NB:__ ```/php/emergency-feed-proxy.php``` takes the state and cache time arguments passed on from the JS file.

### Proxy ###

The map talks to ```emergency-feed-proxy,.php``` which  includes the following functionality:

- fetch third party feed at user defined intervals
- write feed to ```../feeds/```
- return cached feed file
- handle third party errors (see changlog 4.01)

### Dev environment ###

Use Grunt.
Develop in ```src/```, build to ```dist/x.xx```

---------
